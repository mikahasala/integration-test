const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {
    before("Starting server", (done) => {
        server = app.listen(port, () => {
            done();
        });
    });
    describe("Hex to RGB conversion", () => {
        const baseurl = "http://localhost:${port}";
        it("returns status 200", (done) => {
            const url = baseurl + "/hex-to-rgb?red=255&green=0&blue=0";
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    })
    after("Stop server", (done) => {
        server.close();
        done();
    });
});