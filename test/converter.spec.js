// TDD -unit testing

const expect = require ("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redRgb = converter.HexTorgb("#ff0000"); // 255, 0 , 0
            expect(redRgb).to.equal(255, 0, 0)
            const greenRgb = converter.HexTorgb("#00ff00"); // 0, 255 , 0
            expect(greenRgb).to.equal(0, 255, 0)  
            const blueRgb = converter.HexTorgb("#0000ff"); // 0, 0, 255
            expect (blueRgb).to.equal(0, 0, 255)
        })
    })
})