const { request } = require("express");
const express = require("express");
const converter = require("./converter");
const app = express();
const port = 3000;

app.get("/", (req, res) => res.send("Welcome"));

app.get("hex-to-rgb", (req, res) => {
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue, 10);
    res.send(converter.HexTorgb(red, green, blue));
});

if (process.env.NODE_ENV === "test") {
        module.exports = app;
}   else {
    app.listen(port, () => console.log("Server listening"));
}

console.log("NODE_ENV: " + process.env.NODE_ENV);
