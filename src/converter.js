 /**
     * Padding output to match 2 characters always
     * @param {string} hex one or two characters
     *  @returns {string} hex with two characters
     * returns
     */

const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
    /**
     * @param {string} hex 
     * @returns {number} 0-255 
     */

    HexTorgb: (hex) => {
        const redRgb = hex.toString(16)
        const greenRgb = hex.toString(16)
        const blueRgb = hex.toString(16)
        const hexresult = "#" + pad(redRgb) + pad(greenRgb) + pad(blueRgb);
        return hexresult;
    }
};